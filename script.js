let Form = document.querySelector("form");
Form.addEventListener("submit", ValidateForm);

function ValidateForm(event) {
  event.preventDefault();
  let Email = document.getElementById("Email").value;
  let Password = document.getElementById("Password").value;
  let Gender = document.getElementById("Gender").value;
  let Role = document.querySelectorAll("input[type='radio']");
  let Permissions = document.querySelectorAll("input[type='checkbox']");
  const PasswordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,}$/;

  if (!PasswordRegex.test(Password)) {
    alert(
      "Password must of min 6 letter and should contain upper cases letter, lower case letter,digit"
    );
    return;
  }
  let permissionCount = 0;
  Permissions.forEach((Permission) => {
    if (Permission.checked == true) {
      ++permissionCount;
    }
  });
  if (permissionCount < 2) {
    alert("Select at least 2 Permission");
    return;
  }
  DisplayInfo(Email, Password, Gender, Role, Permissions);
}

function DisplayInfo(Email, Password, Gender, Role, Permissions) {
  Form.remove();

  let Card = document.getElementById("card");
  let Card_Email = document.getElementById("Card_Email");
  let Card_Password = document.getElementById("Card_Password");
  let Card_Gender = document.getElementById("Card_Gender");
  let Card_Role = document.getElementById("Card_Role");
  let Card_Permission = document.getElementById("Card_Permission");

  let RoleText;
  Role.forEach((item) => item.checked && (RoleText = item.value));

  let PermissionText = [];

  Permissions.forEach(
    (item) => item.checked && PermissionText.push(item.value)
  );

  Card.style.display = "block";

  Card_Email.innerText = `Email:${Email}`;
  Card_Password.innerText = `Password:${Password}`;
  Card_Gender.innerText = `Gender:${Gender}`;
  Card_Role.innerText = `Role:${RoleText}`;
  Card_Permission.innerText = `Permission:${PermissionText}`;
}
let CardButton = document.getElementById("Card_button");
CardButton.addEventListener("click", () => {
  alert("Confirm button clicked");
});
